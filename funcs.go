package main

import (
	"strconv"
	"time"
)

func Itoa(I int) string {
	return strconv.Itoa(I)
}

func starttime() string {
	now := time.Now()
	year := Itoa(now.Year())
	month := Itoa(int(now.Month()))
	day := Itoa(now.Day())
	hour := Itoa(now.Hour())
	minute := Itoa(now.Minute())
	second := Itoa(now.Second())
	return year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second
}
