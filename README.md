## sigws

![GitHub](https://img.shields.io/github/license/sigws/sigws)
![GitHub repo size](https://img.shields.io/github/repo-size/sigws/sigws)
![GitHub Workflow Status (branch)](https://img.shields.io/github/workflow/status/sigws/sigws/Build/master)

This project comprises the core Golang HTTP abstractions

### Installing

Note that currently this project has been tested on Windows 10/11 and Ubuntu 20.04.* although it likely works on many
other platforms.

But we cross compile Darwin Linux Windows FreeBSD in arm64 and amd64.

Have a try, turn right to [Release Page](https://github.com/sigws/sigws/releases) or [Github Actions](https://github.com/sigws/sigws/actions).

### Build

```go build```

### Download

Go to Release Page or Github Actions and download the software that fits your platform.

Or you can use dll inside your own project, but you should sign my name and my project link on your project.

### How To Use

Just put you web file into `/www`, then run the program with port.
For example `sigws_linux_amd64 8080`.

### License

See [MIT LICENSE](LICENSE).

### Thanks

Thank you so much JetBrains.This is a great encouragement to me

[![JetBrains](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg?_ga=2.81844693.2113755496.1633249320-1809777644.1633249320&_gl=1*brwjbt*_ga*MTgwOTc3NzY0NC4xNjMzMjQ5MzIw*_ga_V0XZL7QHEB*MTYzMzM1MzI4NS43LjEuMTYzMzM1MzI4Ni4w)](https://jb.gg/OpenSource)
