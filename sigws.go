package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var StartTime string
var sugarLogger *zap.SugaredLogger

func InitLogger() {
	StartTime = starttime()

	encoder := getEncoder()
	writeSyncer := getLogWriter()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	// zap.AddCaller()  添加将调用函数信息记录到日志中的功能。
	logger := zap.New(core, zap.AddCaller())
	sugarLogger = logger.Sugar()
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder // 修改时间编码器

	// 在日志文件中使用大写字母记录日志级别
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	// NewConsoleEncoder 打印更符合人们观察的方式
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogWriter() zapcore.WriteSyncer {
	file, _ := os.Create("logs/" + StartTime + ".log")
	return zapcore.AddSync(file)
}

func main() {
	err := os.MkdirAll("logs", os.ModePerm)
	if err != nil {
		fmt.Println("Create logs folder error:", err)
	} else {
		fmt.Println("Create logs folder successfully!")
	}
	InitLogger()

	//TODO: 从文件获取config
	sugarLogger.Info("sigws start!")
	var PORT string
	for k, v := range os.Args {
		if k == 1 {
			PORT = v
		}
	}
	if PORT == "" {
		PORT = "80"
		sugarLogger.Warn("Can't find custom port,using 80......")
	}
	srv := &http.Server{
		Addr:    ":" + PORT,
		Handler: http.FileServer(http.Dir("www")), //配置路由
	}
	go func() {
		sugarLogger.Info("Start listen on " + PORT) //端口写入正常并写入log
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			sugarLogger.Error("listen: ", err)
		} else {
			sugarLogger.Info("Stop serve!")
		}
	}()
	quit := make(chan os.Signal, 1) //捕获Ctrl+C signal并结束
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		sugarLogger.Error("Server shutdown: ", err)
	}
}
